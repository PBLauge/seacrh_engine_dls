﻿using CsvHelper;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using Search_Engine_DLS.DAL.Mappings;
using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Search_Engine_DLS_SeedFinal
{
    internal class Program
    {
        private static ISession _session;

        //private static ISessionFactory Factory { get; set; }
        private static void Main(string[] args)
        {
            var config = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(c => c.FromAppSetting("connectionString")))
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(DocumentMap))));

            Console.WriteLine("Fluently nHibernate configured!");
            Console.WriteLine("Press any key to update/create database");
            Console.ReadKey();

            _session = config.ExposeConfiguration(c => new SchemaUpdate(c).Execute(true, true))
                .BuildConfiguration()
                .BuildSessionFactory()
                .OpenSession();

            Console.WriteLine("Done");
            Console.WriteLine("Running Seed method...");

            Seed();

            Console.WriteLine("Done");
            Console.ReadKey();

        }

        private static void Seed()
        {
            var allYoutubers = getAllYoutubersFromCsv();

            using (var trans = _session.BeginTransaction())
            {
                var youtubers = _session.Query<Youtuber>().ToList();
                foreach (var item in allYoutubers)
                {
                    AddYoutuber(youtubers, item.Name, item.Rank, item.VideoUploads);
                }
                trans.Commit();
            }
        }

        private static List<Youtuber> getAllYoutubersFromCsv()
        {
            using (var reader = new StreamReader("../../data.csv"))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.Delimiter = ",";
                var records = new List<Youtuber>();
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    var record = new Youtuber
                    {
                        Id = default(int),
                        Name = csv.GetField("Name"),
                        Rank = csv.GetField("Rank"),
                        VideoUploads = csv.GetField("VideoUploads")

                    };
                    records.Add(record);
                }
                return records;
            }
        }

        private static void AddYoutuber(List<Youtuber> youtubers, string name, string rank, string videoUploads)
        {
            if (youtubers.All(c => !c.Name.Equals(name)))
            {
                _session.SaveOrUpdate(new Youtuber
                {
                    Name = name,
                    Rank = rank,
                    VideoUploads = videoUploads,
                    Modified = DateTime.UtcNow
                });
                Console.WriteLine($"Added youtuber: {name}");
            }
        }

    }
}
