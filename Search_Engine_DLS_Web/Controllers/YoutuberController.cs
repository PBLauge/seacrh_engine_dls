﻿using Search_Engine_DLS.BLL.Services.Impl;
using Search_Engine_DLS.BLL.Services.Interface;
using Search_Engine_DLS.DAL.Repositories.Impl;
using Search_Engine_DLS.DAL.Repositories.Interface;
using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Search_Engine_DLS_Web.Controllers
{
    public class YoutuberController : ApiController
    {
        YoutuberService _youtuberService = new YoutuberService();


        // GET: api/Youtuber
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //// GET: api/Youtuber/5
        //[HttpGet]
        //[Route("api/Youtuber/{query}")]
        //public List<Youtuber> Get(string query)
        //{
        //    return _youtuberService.SearchByName(query);
        //}

       // GET: api/Youtuber/{query}
        [HttpGet]
        [Route("api/Youtuber/{query?}")]
        public HttpResponseMessage Get(string query)
        {
        if (!string.IsNullOrEmpty(query))
        {
            var response = _youtuberService.SearchByName(query);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

    // POST: api/Youtuber
    public void Post([FromBody]string value)
    {
    }

    // PUT: api/Youtuber/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE: api/Youtuber/5
    public void Delete(int id)
    {
    }
}
}
