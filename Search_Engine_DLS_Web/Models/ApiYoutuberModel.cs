﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Search_Engine_DLS.Entities;

namespace Search_Engine_DLS_Web.Models
{
    public class ApiYoutuberModel
    {
        public string Name { get; set; }
        public string Rank { get; set; }
        public string VideoUploads { get; set; }

    }
}
