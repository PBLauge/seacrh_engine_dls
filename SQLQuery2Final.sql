﻿USE [master]
GO

CREATE DATABASE Search_Engine_DB	
GO

USE Search_Engine_DB
GO

CREATE TABLE Document(
	Id			int			IDENTITY(1,1)	NOT NULL,
	Url			nvarchar(50)			NULL,
	Deleted			bit					NULL,
	Modified		datetime			NULL
	CONSTRAINT PK_Document PRIMARY KEY(Id)
)
GO


CREATE TABLE Term(
	Id			int		IDENTITY(1,1)	 NOT NULL,
	Value 			nvarchar(50)			NULL,
	Type			nvarchar(50)			NULL,
	Deleted			bit					NULL,
	Modified		datetime			NULL
	CONSTRAINT PK_Term PRIMARY KEY(Id)
) 
GO

CREATE TABLE TermDoc(
	DocId			int				NOT NULL,
	TermId			int				NOT NULL,
	Position		nvarchar(50)			NOT NULL
	CONSTRAINT PK_TermDoc PRIMARY KEY(DocId,TermId)
)
	-- Referential Integrity Constraint
ALTER TABLE TermDoc ADD CONSTRAINT FK_Document_TermDoc FOREIGN KEY(DocId)
REFERENCES Document(Id)

ALTER TABLE TermDoc ADD CONSTRAINT FK_Term_TermDoc FOREIGN KEY(TermId)
REFERENCES Term(Id)