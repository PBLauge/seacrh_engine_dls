﻿using Search_Engine_DLS.BLL.Services.Interface;
using Search_Engine_DLS.DAL.Repositories.Impl;
using Search_Engine_DLS.DAL.Repositories.Interface;
using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;

namespace Search_Engine_DLS.BLL.Services.Impl
{
    public class YoutuberService : IYoutuberService
    {
        private readonly NhYoutuberRepository _youtuberRepository = new NhYoutuberRepository();

        public YoutuberService()
        {
        }

        public YoutuberService(NhYoutuberRepository youtuberRepo)
        {
            _youtuberRepository = youtuberRepo;
        }

        public List<Youtuber> SearchByName(string searchQuery)
        {
            List<Youtuber> youtubers = new List<Youtuber>();
            var results = _youtuberRepository.SearchForName(searchQuery);
            if (results != null)
            {
                foreach (var result in results)
                {
                    youtubers.Add(result);
                }
                return youtubers;
            }
            else return null;
        }

    }
}
