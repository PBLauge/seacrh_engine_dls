﻿using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search_Engine_DLS.BLL.Services.Interface
{
    public interface IYoutuberService
    {
       List<Youtuber> SearchByName(string searchQuery);
    }
}
