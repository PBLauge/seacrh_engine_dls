﻿using Search_Engine_DLS.BLL.Services.Impl;
using Search_Engine_DLS.BLL.Services.Interface;
using Search_Engine_DLS.DAL.Repositories.Impl;
using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Search_Engine_DLS
{
    public partial class UI : Form
    {

        //SqlConnection connection = new SqlConnection(@"Data Source=LAPTOP-D822DC95\MSSQLSERVER01;Initial Catalog=Search_Engine_DB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        //SqlCommand command;
        //SqlDataAdapter adapter;
        //DataTable table;

        private readonly IYoutuberService _youtuberService;
        List<Youtuber> results;

        public UI()
        {
            InitializeComponent();
            _youtuberService = new YoutuberService(new NhYoutuberRepository());
           
        }

        public UI(IYoutuberService yService)
        {
            _youtuberService = yService;
            InitializeComponent();
        }

        //public void SearchData(string valueToSearch)
        //{

        //    string query = "SELECT * FROM Youtuber WHERE CONCAT('Name','Rank','VideoUploads') like '%" + valueToSearch + "%'";
        //    command = new SqlCommand(query, connection);
        //    adapter = new SqlDataAdapter(command);
        //    table = new DataTable();
        //    adapter.Fill(table);
        //    dataGridView1.DataSource = table;
        //}


        // 1 button search
        private void button1_Click_1(object sender, EventArgs e, string name)
        {
           
           
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string valueToSearch = search_textBox.Text.ToString();
                results = _youtuberService.SearchByName(valueToSearch);
                Console.WriteLine(results);
                if (results != null)
                {
                    listView.Items.Clear();

                    foreach (var result in results)
                    {
                        ListViewItem item = listView.Items.Add(result.Rank);
                        item.SubItems.Add(result.Name);
                        item.SubItems.Add(result.VideoUploads);
                        //listView.Items.Add(result.Rank, 0);
                        //listView.Items.Add(result.Name, 1);
                        //listView.Items.Add(result.VideoUploads, 2);
                    }
                }


            }
            catch(Exception error)
            {
                Console.WriteLine(error);
            }
           
           
        }

        // 1 searchbox
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void UI_Load(object sender, EventArgs e)
        {

        }

        // listView
        private void listView1_column(object sender, EventArgs e)
        {
          
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
