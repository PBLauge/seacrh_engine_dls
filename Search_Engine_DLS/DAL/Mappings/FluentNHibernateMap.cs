﻿using FluentNHibernate.Mapping;
using Search_Engine_DLS.Entities;


namespace Search_Engine_DLS.DAL.Mappings
{
    public abstract class FluentNHibernateMap<T> : ClassMap<T> where T: Entity
    {
        protected FluentNHibernateMap()
        {
            Id(x => x.Id)
                .GeneratedBy.Identity();
            Map(x => x.Deleted);
            Map(x => x.Modified);
            Cache.ReadWrite();

        }
    }
}
