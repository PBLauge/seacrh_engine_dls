﻿
using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search_Engine_DLS.DAL.Mappings
{
    public class TermDocMap : FluentNHibernateMap<TermDoc>
    {
        public TermDocMap()
        {
            Map(x => x.Position);
            References(x => x.Document)
                .Cascade.SaveUpdate()
                .ForeignKey("FK_DocId");
            References(x => x.Term)
                .Cascade.SaveUpdate()
                .ForeignKey("FK_TermId");
        }
       
    }
}
