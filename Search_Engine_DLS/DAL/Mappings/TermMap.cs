﻿using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search_Engine_DLS.DAL.Mappings
{
    public class TermMap : FluentNHibernateMap<Term>
    {
        public TermMap()
        {
            Map(x => x.Value);
            Map(x => x.Type);
        }
    }
}
