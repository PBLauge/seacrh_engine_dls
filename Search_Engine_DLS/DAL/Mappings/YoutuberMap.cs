﻿using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search_Engine_DLS.DAL.Mappings
{
    public class YoutuberMap : FluentNHibernateMap<Youtuber>
    {
        public YoutuberMap()
        {
            Map(x => x.Name);
            Map(x => x.Rank);
            Map(x => x.VideoUploads);
        }
    }
}
