﻿using Search_Engine_DLS.Entities;
using Search_Engine_DLS.DAL.Interface;

namespace Search_Engine_DLS.DAL.Interface
{
    public interface IDocumentRepository : IRepository<Document>
    {

    }
}
