﻿using Search_Engine_DLS.DAL.Interface;
using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search_Engine_DLS.DAL.Repositories.Interface
{
    public interface IYoutuberRepository: IRepository<Youtuber>
    {
        List<Youtuber> SearchForName(string searchQuery);
    }
}
