﻿using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Search_Engine_DLS.DAL.Interface
{
    public interface IRepository
    {
    }

    public interface IRepository<TEntity> : IRepository where TEntity : Entity
    {
        int Count();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();
    }
}
