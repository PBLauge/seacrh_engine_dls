﻿using Search_Engine_DLS.DAL.Impl;
using Search_Engine_DLS.DAL.Repositories.Interface;
using Search_Engine_DLS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search_Engine_DLS.DAL.Repositories.Impl
{
    public class NhYoutuberRepository : NhRepositoryBase<Youtuber>, IYoutuberRepository
    {
        public List<Youtuber> SearchForName(string searchQuery)
        {
            return Query().Where(n => n.Name.Contains(searchQuery)).ToList();
        }


    }
}
