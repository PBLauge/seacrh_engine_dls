﻿using NHibernate;
using Search_Engine_DLS.Entities;
using Search_Engine_DLS.DAL.Interface;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Diagnostics;
using NHibernate.Linq;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace Search_Engine_DLS.DAL.Impl
{
    /// <summary>
    /// Base class for all repositories those uses Fluent NHibernate.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class NhRepositoryBase<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        /// <summary>
        /// Gets the Nhibernate session object to perform database operations.
        /// </summary>
        protected ISession Session => NHibernateSessionManager.GetCurrentSession();

        /// <summary>
        /// Used to get a IQueryable that is used to retrive entities from entire table.
        /// </summary>
        /// <returns>IQueryable to be used to select entities from database</returns>
        public IQueryable<TEntity> Query()
        {
            return Session.Query<TEntity>().Cacheable().CacheMode(CacheMode.Normal).Where(e => !e.Deleted);
        }


        public int Count()
        {

            return Query().Count();

        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public TEntity Get(int id)
        {
            return Session.Get<TEntity>(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Session.Query<TEntity>().ToList();

        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Query().FirstOrDefault(predicate);
        }

        /// <summary>
        /// Save or update an entity
        /// </summary>
        /// <param name="entity">Entity</param>
        public void SaveOrUpdate(TEntity entity)
        {
            using (var trans = Session.BeginTransaction())
            {
                try
                {
                    entity.Modified = DateTime.UtcNow;
                    Session.SaveOrUpdate(entity);
                    trans.Commit();
                }
                catch
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

    }
}
