﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Context;
using System;
using System.Web;

namespace Search_Engine_DLS.DAL
{
    public class NHibernateSessionManager
    {
        private static ISessionFactory Factory;
        public static string ConnectionString { get; set; }

        static NHibernateSessionManager()
        {
            ConnectionString = String.Empty;
            
        }
     

        private static ISessionFactory GetFactory<T>() where T : ICurrentSessionContext
        {
            return Fluently.Configure()
                  .Database(MsSqlConfiguration.MsSql2012
                  .ConnectionString(@"Server=tcp:lukgf.database.windows.net,1433;Initial Catalog=SearchEngineDB;Persist Security Info=False;User ID=StaticUser;Password=!aProblemOfMine26;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
                  .Mappings(m => m.FluentMappings.AddFromAssemblyOf<NHibernateSessionManager>())
                  .CurrentSessionContext<T>().BuildSessionFactory();

        }

        /// <summary>
        /// Gets the current session.
        /// </summary>
        public static ISession GetCurrentSession()
        {
            if (Factory == null)
                Factory = HttpContext.Current != null ? GetFactory<WebSessionContext>() : GetFactory<ThreadStaticSessionContext>();

            try
            {
                if (CurrentSessionContext.HasBind(Factory))
                    return Factory.GetCurrentSession();
            }
            catch
            {
                Factory = GetFactory<ThreadStaticSessionContext>();
            }

            var session = Factory.OpenSession();
            CurrentSessionContext.Bind(session);

            return session;
        }

        /// <summary>
        /// Closes the session.
        /// </summary>
        public static void CloseSession()
        {
            if (Factory != null && CurrentSessionContext.HasBind(Factory))
            {
                var session = CurrentSessionContext.Unbind(Factory);
                session.Close();
            }
        }

        /// <summary>
        /// Commits the session.
        /// </summary>
        /// <param name="session">The session.</param>
        public static void CommitSession(ISession session)
        {
            try
            {
                session.Transaction.Commit();
            }
            catch (Exception)
            {
                session.Transaction.Rollback();
                throw;
            }
        }
    }
}
