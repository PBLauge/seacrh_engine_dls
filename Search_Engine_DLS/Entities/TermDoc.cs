﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search_Engine_DLS.Entities
{
    public class TermDoc : Entity
    {
        public virtual string Position { get; set; }
        public virtual Document Document {get; set;}
        public virtual Term Term { get; set; }


    }
}
