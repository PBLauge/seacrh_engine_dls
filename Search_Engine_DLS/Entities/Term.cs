﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search_Engine_DLS.Entities
{
    public class Term : Entity
    {

        public virtual string Value { get; set; }
        public virtual string Type { get; set; }
    }
}
