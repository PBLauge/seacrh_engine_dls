﻿namespace Search_Engine_DLS.Entities
{
    /// <summary>
    /// Defines interface for base entity type.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Primary key of the entity
        /// </summary>
        int Id { get; set; }
    }
}
