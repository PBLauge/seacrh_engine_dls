﻿namespace Search_Engine_DLS.Entities
{
    public class Youtuber : Entity
    {
        public virtual string Name { get; set; }

        public virtual string Rank { get; set; }

        public virtual string VideoUploads { get; set; }
    }
}
