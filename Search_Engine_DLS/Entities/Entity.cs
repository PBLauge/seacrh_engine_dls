﻿using System;
namespace Search_Engine_DLS.Entities
{
    public class Entity : IEntity
    {
        public virtual int Id { get; set; }
        public virtual bool Deleted { get; set; }
        public virtual DateTime Modified { get; set; }

    }
}
