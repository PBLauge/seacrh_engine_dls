﻿using Search_Engine_DLS.DAL;
using Search_Engine_DLS.DAL.Impl;
using Search_Engine_DLS.Entities;
using System;
using System.Configuration;
using System.Data.Common;
using System.IO;
using System.Windows.Forms;

namespace Search_Engine_DLS
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
           

          /*  string provider = ConfigurationManager.AppSettings["provider"];
            string connectionString = ConfigurationManager.AppSettings["connectionString"];

            DbProviderFactory factory = DbProviderFactories.GetFactory(provider);

            // test for seeding data in Db
            using (var session = NHibernateSessionManager.GetCurrentSession())
            {
                var term = new Term { Type = "testReference", Value = "hello", Deleted = false, Modified = DateTime.UtcNow };
                var doc = new Document { Url = "test", Deleted = false, Modified = DateTime.UtcNow };

                session.SaveOrUpdate(term);
                session.SaveOrUpdate(doc);
            }*/

            // execute the UI part
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new UI());
        }
    }
}
